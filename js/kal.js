const library_name = "K";

~(function (global, factory) {
  "object" === typeof exports && "undefined" !== typeof module
    ? (module.exports = factory())
    : "function" === typeof define && define.amd
    ? define(factory)
    : (global[library_name] = factory());
})(this, function () {
  "use strict";

  let K = {};

  const should_expose_utils = true;
  const should_K_act_as_DOM = true;
  const should_K_Objects_be_sealed = true;

  K = function (e, fo) {
    if ("string" !== typeof e || fo === true || !should_K_act_as_DOM)
      return new K.Object(e, fo);
    if ("undefined" === typeof document) return new K.Object(e, true);
    if (should_K_act_as_DOM) {
      if (e[0] === "<" && e[e.length - 1] === ">") {
        const elt = e.substr(1, e.length - 2);
        if ("object" === typeof fo) {
          return K.Utils.create(elt, fo);
        } else {
          return K.Utils.create(elt);
        }
      }
      return K.Utils.select(e);
    }
  };

  K.version = "1.1.10+2";

  K.Object = class {
    constructor(v, f) {
      if (v instanceof K.Object) return v;
      this._value = v;
      Object.defineProperties(this, {
        _constructor: {
          value: v ? v.constructor.name : null,
          writable: false,
        },
        _forced: {
          value: !!f,
          writable: false,
        },
        _primitive: {
          value: typeof v,
          writable: false,
        },
      });
      if (should_K_Objects_be_sealed) Object.seal(this);
    }

    get(prop) {
      if (this.primitive === "object") {
        return prop in this.val ? this.val[prop] : undefined;
      } else return undefined;
    }

    get val() {
      return this._value;
    }

    set val(v) {
      this._value = v;
    }

    get type() {
      return this._constructor;
    }

    get isForced() {
      return this._forced;
    }

    get primitive() {
      return this._primitive;
    }

    addClass(classes) {
      if (undefined === this.get("className")) throw new Error(K.Error.NOT_DOM);
      this.val.className += ` ${classes}`;
      return this;
    }

    after(html) {
      if (undefined === this.get("insertAdjacentHTML"))
        throw new Error(K.Error.NOT_DOM);
      this.val.insertAdjacentHTML("afterend", html);
      return this;
    }

    append(node) {
      if (undefined === this.get("appendChild"))
        throw new Error(K.Error.NOT_DOM);
      this.val.appendChild(node.val);
      return this;
    }

    attr(name) {
      if (undefined === this.get("getAttribute"))
        throw new Error(K.Error.NOT_DOM);
      return this.val.getAttribute(name);
    }

    before(html) {
      if (undefined === this.get("insertAdjacentHTML"))
        throw new Error(K.Error.NOT_DOM);
      this.val.insertAdjacentHTML("beforebegin", html);
      return this;
    }

    children() {
      if (undefined === this.get("childNodes"))
        throw new Error(K.Error.NOT_DOM);
      return new K.Object(this.val.childNodes);
    }

    clone() {
      if (undefined === this.get("cloneNode")) throw new Error(K.Error.NOT_DOM);
      return new K.Object(this.val.cloneNode(true));
    }

    css(o) {
      if (undefined === this.get("setAttribute"))
        throw new Error(K.Error.NOT_DOM);
      if (typeof o === "string") this.val.setAttribute("style", o);
      else {
        let styleStr = "";
        Object.keys(o).forEach((k) => {
          styleStr += `${k}:${o[k]};`;
        });
        this.val.setAttribute("style", styleStr);
      }
      return this;
    }

    empty() {
      if (undefined === this.get("innerHTML")) throw new Error(K.Error.NOT_DOM);
      this.val.innerHTML = "";
      return this;
    }

    first() {
      return new K.Object(this.val[0]);
    }

    firstChild() {
      if (undefined === this.get("childNodes"))
        throw new Error(K.Error.NOT_DOM);
      const firstChild = this.val.childNodes[0];
      return new K.Object(firstChild ? firstChild : null);
    }

    html(html, append = false) {
      if (undefined === this.get("innerHTML")) throw new Error(K.Error.NOT_DOM);
      if (append) this.val.innerHTML += html;
      else this.val.innerHTML = html;
      return this;
    }

    lastChild() {
      if (undefined === this.get("childNodes"))
        throw new Error(K.Error.NOT_DOM);
      const lastChild = this.val.childNodes[this.val.childNodes.length - 1];
      return new K.Object(lastChild ? lastChild : null);
    }

    next() {
      if (undefined === this.get("nextElementSibling"))
        throw new Error(K.Error.NOT_DOM);
      return new K.Object(this.val.nextElementSibling);
    }

    on(event, callback) {
      if (undefined === this.get("addEventListener"))
        throw new Error(K.Error.NOT_DOM);
      this.val.addEventListener(event, callback.bind(this));
      return this;
    }

    parent() {
      if (undefined === this.get("parentNode"))
        throw new Error(K.Error.NOT_DOM);
      return new K.Object(this.val.parentNode);
    }

    prepend(node) {
      if (undefined === this.get("insertBefore"))
        throw new Error(K.Error.NOT_DOM);
      const firstChild = this.val.childNodes[0];
      this.val.insertBefore(node.val, firstChild ? firstChild : null);
      return this;
    }

    prev() {
      if (undefined === this.get("previousElementSibling"))
        throw new Error(K.Error.NOT_DOM);
      return new K.Object(this.val.previousElementSibling);
    }

    remove() {
      if (undefined === this.get("removeChild"))
        throw new Error(K.Error.NOT_DOM);
      this.val.parentNode.removeChild(this.val);
    }

    removeClass(c) {
      if (undefined === this.get("classList")) throw new Error(K.Error.NOT_DOM);
      this.val.classList.remove(c);
      return this;
    }

    scroll(options) {
      if (undefined === this.get("scrollIntoView"))
        throw new Error(K.Error.NOT_DOM);
      this.val.scrollIntoView(options);
      return this;
    }

    trigger(event) {
      if (undefined === this.get("dispatchEvent"))
        throw new Error(K.Error.NOT_DOM);
      this.val.dispatchEvent(event);
    }

    set(o) {
      if (undefined === this.get("setAttribute"))
        throw new Error(K.Error.NOT_DOM);
      Object.keys(o).forEach((k) => {
        this.val.setAttribute(k, o[k]);
      });
      return this;
    }

    siblings() {
      if (undefined === this.get("parentNode"))
        throw new Error(K.Error.NOT_DOM);
      return new K.Object(
        Array.prototype.filter.call(
          this.val.parentNode.children,
          (child) => child !== this.val
        )
      );
    }

    text(txt) {
      if (undefined === this.get("textContent"))
        throw new Error(K.Error.NOT_DOM);
      this.val.textContent = txt;
      return this;
    }

    avg() {
      this.val = K.Utils.avg(this);
      return this;
    }

    chunk(chunkSize) {
      this.val = K.Utils.chunk(this, chunkSize);
      return this;
    }

    clamp(start, stop) {
      this.val = K.Utils.clamp(this, start, stop);
      return this;
    }

    clean() {
      this.val = K.Utils.clean(this);
      return this;
    }

    concat(arr2) {
      this.val = K.Utils.concat(this, arr2);
      return this;
    }

    count(toConsole = true) {
      K.Utils.count(this, toConsole);
      return this;
    }

    each(callback, force = false) {
      this.val.forEach((item, index, thisArr) =>
        callback(new K.Object(item, force), index, thisArr)
      );
      return this;
    }

    flatten() {
      this.val = K.Utils.flatten(this);
      return this;
    }

    join(separator) {
      this.val = K.Utils.join(this, separator);
      return this;
    }

    map(start1, stop1, start2, stop2) {
      this.val = K.Utils.map(this, start1, stop1, start2, stop2);
      return this;
    }

    partition(isValid) {
      this.val = K.Utils.partition(this, isValid);
      return this;
    }

    shuffle() {
      this.val = K.Utils.shuffle(this);
      return this;
    }

    sort(descending = false) {
      this.val = K.Utils.sort(this, descending);
      return this;
    }

    split(separator) {
      this.val = K.Utils.split(this, separator);
      return this;
    }

    sum() {
      this.val = K.Utils.sum(this);
      return this;
    }

    toArray() {
      this.val = K.Utils.toArray(this);
      return this;
    }

    toNum() {
      this.val = K.Utils.toNum(this);
      return this;
    }

    transpose() {
      this.val = K.Utils.transpose(this);
      return this;
    }
  };

  K.Utils = {
    avg(arr) {
      const a = K.Utils.val(arr);
      return sum(a) / a.length;
    },

    chunk(arr, chunkSize) {
      const array = K.Utils.val(arr);
      return Array(Math.ceil(array.length / chunkSize))
        .fill()
        .map((_, index) => index * chunkSize)
        .map((begin) => array.slice(begin, begin + chunkSize));
    },

    clamp(n, start, stop) {
      const v = K.Utils.val(n);
      return v < start ? start : v > stop ? stop : v;
    },

    clean(arr) {
      return K.Utils.val(arr).filter(Boolean);
    },

    concat(arr1, arr2) {
      const newArr = [];
      newArr.push.apply(newArr, K.Utils.val(arr1));
      newArr.push.apply(newArr, K.Utils.val(arr2));
      return newArr;
    },

    count(arr, toConsole = true) {
      const res = {};
      K.Utils.val(arr).forEach((item) => {
        res[item] = res[item] ? (res[item] += 1) : 1;
      });
      if (toConsole) console.table(res);
      return res;
    },

    copy(text) {
      return navigator.clipboard.writeText(text);
    },

    create(elt, options) {
      const node = new K.Object(document.createElement(elt));
      if (options) node.set(options);
      return node;
    },

    delay(time) {
      return new Promise((resolve) => setTimeout(resolve, time));
    },

    each(array, callback) {
      array.forEach((item, index, thisArr) => callback(item, index, thisArr));
    },

    eq(value, other) {
      const v = K.Utils.val(value);
      const o = K.Utils.val(other);
      return v === o || (v !== v && o !== o);
    },

    event(name, options) {
      return new CustomEvent(name, options);
    },

    extend(oldObj) {
      let newObj = oldObj;
      if (oldObj && typeof oldObj === "object") {
        newObj =
          Object.prototype.toString.call(oldObj) === "[object Array]" ? [] : {};
        for (let i in oldObj) {
          newObj[i] = K.Utils.extend(oldObj[i]);
        }
      }
      return newObj;
    },

    flatten(arr) {
      return K.Utils.val(arr).reduce((flat, toFlatten) => {
        return flat.concat(
          Array.isArray(toFlatten) ? K.Utils.flatten(toFlatten) : toFlatten
        );
      }, []);
    },

    generateAes(size = 16) {
      const a = "0123456789abcdef";
      let k = "";
      K.Utils.repeat(size, () => (k += K.Utils.random(a)));
      return k;
    },

    generateUuid() {
      return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
        (
          c ^
          (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
        ).toString(16)
      );
    },

    gt(x, y) {
      return K.Utils.val(x) > K.Utils.val(y);
    },

    gte(x, y) {
      return K.Utils.val(x) >= K.Utils.val(y);
    },

    join(array, separator = ",") {
      return K.Utils.val(array).join(separator);
    },

    loadJson(filename) {
      return fetch(`${filename}.json`).then((response) => response.json());
    },

    loadText(filename) {
      return fetch(`${filename}.txt`).then((response) => response.text());
    },

    log() {
      console.log(...arguments);
    },

    lt(x, y) {
      return K.Utils.val(x) < K.Utils.val(y);
    },

    lte(x, y) {
      return K.Utils.val(x) <= K.Utils.val(y);
    },

    map(n, s1, e1, s2, e2) {
      return ((kal.Utils.val(n) - s1) / (e1 - s1)) * (e2 - s2) + s2;
    },

    max(...args) {
      return Math.max(...K.Utils.flatten(args));
    },

    min(...args) {
      return Math.min(...K.Utils.flatten(args));
    },

    params() {
      const search = window.location.search.slice(1).split("&");
      const params = {};
      search.forEach((item) => {
        item = item.split("=");
        params[item[0]] = item[1];
      });
      return params;
    },

    partition(arr, isValid) {
      return K.Utils.val(arr).reduce(
        ([pass, fail], elem) => {
          return isValid(elem)
            ? [[...pass, elem], fail]
            : [pass, [...fail, elem]];
        },
        [[], []]
      );
    },

    random(max) {
      function rng() {
        if ("undefined" !== typeof crypto) {
          const a = crypto.getRandomValues(new Uint32Array(2));
          const m = a[0] * Math.pow(2, 20) + (a[1] >>> 12);
          return m * Math.pow(2, -52);
        }
        return Math.random();
      }

      if (!max) return rng();
      return typeof max === "number"
        ? ~~(rng() * max)
        : max[~~(rng() * max.length)];
    },

    randomHex() {
      return (
        "#" +
        ("000000" + (~~(K.Utils.random() * (1 << 24))).toString(16)).slice(-6)
      );
    },

    repeat(n, func) {
      return new Promise((resolve) => {
        const r = [];
        for (let i = 1; i <= n; i++) {
          r.push(func(i));
        }
        resolve(r);
      });
    },

    select(elt) {
      const query = document.querySelectorAll(elt);
      return query.length == 1 ? new K.Object(query[0]) : new K.Object(query);
    },

    shuffle(arr) {
      const a = K.Utils.val(arr).slice();
      for (let i = a.length - 1; i > 0; i--) {
        const j = K.Utils.random(i + 1);
        [a[i], a[j]] = [a[j], a[i]];
      }
      return a;
    },

    sort(array, descending = false) {
      const arr = K.Utils.val(array);
      return descending ? arr.sort((a, b) => b - a) : arr.sort((a, b) => a - b);
    },

    split(string, separator = "") {
      return K.Utils.val(string).split(separator);
    },

    sum(arr) {
      return K.Utils.val(arr).reduce((a, b) => a + b);
    },

    toArray(item) {
      return Array.from(K.Utils.val(item));
    },

    toNum(arr) {
      return K.Utils.val(arr).map(Number);
    },

    transpose(arr) {
      const array = K.Utils.val(arr);
      return array[0].map((_, colIndex) => array.map((row) => row[colIndex]));
    },

    val(elt) {
      return elt instanceof K.Object ? elt.val : elt;
    },
  };

  if (should_expose_utils) {
    Object.keys(K.Utils).forEach((f) => {
      K[f] = K.Utils[f];
    });
  }

  K.Error = Object.freeze({
    INVALID_ARGS: "Invalid arguments",
    INVALID_OP: "Invalid operation",
    NOT_DOM: "This object doesn't seem to be a DOM element",
  });

  return K;
});
