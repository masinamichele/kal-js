/**
 * This script is used to load the documentation
 * and stylize the page.
 * It is NOT part of the library.
 */

function parseLink(l) {
  return l.match(/(.+)\(/)[1];
}

K("#logo").html('kal<span style="color:red">.</span>js');

K("#download").text(`Download ${K.version}`);

K.log("Hello, traveller! You can try kal.js in this console.");

K.loadJson("documentation/doc")
  .then(json => {
    let div = K("#documentation");
    let nav = K("#sidenav");

    K.each(json.utils, item => {
      let title = K("<div>", { class: "doc-title" });

      let parsed = parseLink(item.title);

      let anchor = K("<a>", {
        id: parsed,
        href: `#${parsed}`
      }).text(parsed.length > 1 ? `K.${item.title}` : item.title);
      title.append(anchor);
      div.append(title);

      if (parsed.length > 1) {
        let navLink = K("<a>", {
          href: `#${parsed}`
        });
        let span1 = K("<span>", { style: "color:#aaa;" }).text("K.");
        let span2 = K("<span>").text(parsed);
        navLink.append(span1).append(span2);
        nav.append(navLink);
      } else {
        let navLink = K("<a>", {
          href: `#${parsed}`
        }).text(parsed);
        nav.append(navLink);
      }

      let description = K("<div>", {
        class: "doc-desc"
      }).html(item.description);
      div.append(description);

      let innerDiv = K("<div>");

      let exampleBox = K("<pre>");
      let exampleCode = K("<code>", { "data-language": "javascript" });

      K.each(item.examples, ex => {
        exampleCode.html(`${ex.function}\n`, true);

        K.each(ex.results, r => {
          exampleCode.html(`// ${r}\n`, true);
        });
      });

      exampleBox.append(exampleCode);
      innerDiv.append(exampleBox);
      div.append(innerDiv);
    });

    K('a[href^="#"').each(a => {
      a.on("click", function(e) {
        e.preventDefault();
        window.location.hash = this.attr("href");
      });
    });

    K("a[id]").each(a => {
      a.on("click", function(e) {
        K.copy(`${window.location.href.match(/(.*)#/)[1]}${this.attr("href")}`);
      });
    });

    if (window.location.hash) {
      K(window.location.hash).scroll({
        behavior: "smooth"
      });
    }
  })
  .catch(() => {
    K("#documentation").text("Unable to load documentation.");
  });

K(window).on("scroll", function() {
  let jumbotron = K("#jumbotron");
  let logo = K("#logo");
  let sidenav = K("#sidenav");

  if (this.get("scrollY") > 0) {
    jumbotron.css({ height: "40px" });
    logo.css({ "font-size": "35px" });
    sidenav.css({
      top: "64px",
      height: "calc(100vh - 64px)"
    });
  } else {
    jumbotron.css({ height: "100px;" });
    logo.css({ "font-size": "70px;" });
    sidenav.css({ top: "124px", height: "calc(100vh - 124px)" });
  }
});
